FROM node:8.12.0

RUN npm install -g node-sass --unsafe-perm

RUN npm rebuild node-sass --force
